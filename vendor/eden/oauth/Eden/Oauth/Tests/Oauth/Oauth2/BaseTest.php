<?php //-->
/*
 * This file is part of the Oauth package of the Eden PHP Library.
 * (c) 2013-2014 Openovate Labs
 *
 * Copyright and license information can be found at LICENSE
 * distributed with this package.
 */

class Eden_Oauth_Tests_Oauth_Oauth2_BaseTest extends \PHPUnit_Framework_TestCase
{
    public function testGetMeta()
    {
    }

    public function testAutoApprove()
    {
    }

    public function testForceApprove()
    {
    }

    public function testSetState()
    {
    }

    public function testSetScope()
    {
    }

    public function testSetDisplay()
    {
    }

    public function testIsJson()
    {
    }

}
