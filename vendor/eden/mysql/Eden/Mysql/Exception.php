<?php //-->
/*
 * This file is part of the Mysql package of the Eden PHP Library.
 * (c) 2013-2014 Openovate Labs
 *
 * Copyright and license information can be found at LICENSE
 * distributed with this package.
 */

namespace Eden\Mysql;

use Eden\Sql\Exception as SqlException;

/**
 * Sql Errors
 *
 * @vendor Eden
 * @package Mysql
 * @author Christian Blanquera cblanquera@openovate.com
 */
class Exception extends SqlException
{
}