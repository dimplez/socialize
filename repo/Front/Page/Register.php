<?php //-->
/*
 * This file is part of the Openovate Labs Inc. framework library
 * (c) 2013-2014 Openovate Labs
 *
 * Copyright and license information can be found at LICENSE
 * distributed with this package.
 */

namespace Front\Page;

/**
 * The base class for any class that defines a view.
 * A view controls how templates are loaded as well as 
 * being the final point where data manipulation can occur.
 *
 * @vendor Openovate
 * @package Framework
 */
class Register extends \Page 
{
	protected $title = 'Register';

	private function save_reg()
	{
		$coll = $this->connect();

		// Get the Entered Username
		$getUsername = $coll->find( array('Username' => $_POST['username']) );
		$getUsername = iterator_to_array($getUsername, false);

		// If it's already in the DB, return an error
		if($getUsername) {
			return array(
				'error' => array(
					'full_name' => $_POST['full_name'],
					'username' 	=> $_POST['username'],
					'email'		=> $_POST['email']
				)
			);
		}

		// Prepare the Values
		$reg_info = array(
			'Full Name'  => $_POST['full_name'],
			'Username' 	 => $_POST['username'],
			'Email' 	 => $_POST['email'],
			'Profile' 	 => 'upload/default.jpg',
			'Password' 	 => md5($_POST['password'])
		);

		// Save it
		$coll->insert($reg_info);

		header('Location: /login', true, 302);
		exit;
	}

	public function getVariables()
	{
		$method = $_SERVER['REQUEST_METHOD'];

		switch ($method) {
			case 'POST':
				return $this->save_reg();
			case 'GET':
				return array();
		}
	}
}