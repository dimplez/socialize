<?php //-->
/*
 * This file is part of the Openovate Labs Inc. framework library
 * (c) 2013-2014 Openovate Labs
 *
 * Copyright and license information can be found at LICENSE
 * distributed with this package.
 */

namespace Front\Page;

/**
 * The base class for any class that defines a view.
 * A view controls how templates are loaded as well as 
 * being the final point where data manipulation can occur.
 *
 * @vendor Openovate
 * @package Framework
 */
class Home extends \Page 
{
	protected $title = 'Front';

	public function render()
	{
		if(!isset($_SESSION['logged'])) {
            header('Location: /');
            exit;
    	}

    	return parent::render();
	}
}