<?php //-->
/*
 * This file is part of the Openovate Labs Inc. framework library
 * (c) 2013-2014 Openovate Labs
 *
 * Copyright and license information can be found at LICENSE
 * distributed with this package.
 */

namespace Front\Page;

/**
 * The base class for any class that defines a view.
 * A view controls how templates are loaded as well as 
 * being the final point where data manipulation can occur.
 *
 * @vendor Openovate
 * @package Framework
 */
class Login extends \Page 
{
	protected $title = 'Login';

	private function login()
	{
		$coll = $this->connect();
		$login = $coll->findOne(array('Username' => $_POST['username'], 'Password' => md5($_POST['password']) ));

		if($login) {
			$_SESSION['logged'] = $login['_id'];

			header('Location: /home');
			exit;
		}

		return array('login_error' => 'error');
	}

	public function getVariables()
	{
		$method = $_SERVER['REQUEST_METHOD'];

		switch ($method) {
			case 'POST':
				return $this->login();
				break;
			case 'GET':
				return array();
		}
	}
}