<?php //-->
/*
 * This file is part of the Openovate Labs Inc. framework library
 * (c) 2013-2014 Openovate Labs
 *
 * Copyright and license information can be found at LICENSE
 * distributed with this package.
 */

namespace Front\Page;

/**
 * The base class for any class that defines a view.
 * A view controls how templates are loaded as well as 
 * being the final point where data manipulation can occur.
 *
 * @vendor Openovate
 * @package Framework
 */
class Settings extends \Page 
{

	protected $title = 'Front';

	private function update_settings()
	{
		$coll = $this->connect();

		// Query DB, Get the User Info via SESSION
		$setinfo = $coll->findOne( array('_id' => $_SESSION['logged']) );

		// Query DB via $_POST['username']
		$un = $coll->find( array('Username' => $_POST['username']) );
		$un = iterator_to_array($un, false);
		
		// If it has Match then Exit
		if($un) {
			if($un[0]['Username'] !== $setinfo['Username']) {
				// Return an Error Array
				return array(
					'error' => array(
						'full_name' => $_POST['full_name'],
						'username' 	=> $_POST['username'],
						'email'		=> $_POST['email'],
						'profile'	=> $setinfo['Profile'],
						'password'	=> $un[0]['Password']
					)
				);
			}
		}

		// If it has no Match
		$pw = $setinfo['Password'];
		// If Input Password has value
		if($_POST['password']) {
			$pw = md5($_POST['password']);
		}
		
		// If input:file has value
		if($_FILES["profileimage"]['name'] != '') {
			// Delete the previous Image
			if($setinfo['Profile'] != 'upload/default.jpg') {
				unlink($setinfo['Profile']);
			}

			// Add the Link to this variable
			$link = 'upload/'. $_FILES["profileimage"]["name"];

			// Move the Image to the Folder
			move_uploaded_file($_FILES["profileimage"]["tmp_name"], $link);
		}

		// Prepare the Values
		$update_info = array(
			'$set' => array(
				'Full Name' 	=> $_POST['full_name'],
				'Username'		=> $_POST['username'],
				'Email'			=> $_POST['email'],
				'Profile'		=> $link ? $link : $setinfo['Profile'],
				'Password'		=> $pw
			)
		);
		
		// Save it
		$coll->update(array('_id' => $_SESSION['logged']), $update_info);

		// Redirect
		header('Location: /settings', true, 302);
		exit;
	}

	// GET: Query the logged in User via Session
	public function getInfo()
	{
		$coll = $this->connect();

		$getSettings = $coll->findOne(array('_id' => $_SESSION['logged']));

		return array(
			'settings' 	=> $getSettings
		);
	}

	public function getVariables()
	{
		if(!isset($_SESSION['logged'])) {
            header('Location: /');
            exit;
    	}

		$method = $_SERVER['REQUEST_METHOD'];

		switch ($method) {
			case 'POST':
				return $this->update_settings();
			case 'GET':
				return $this->getInfo();
		}
	}
}
