<?php if( $_SERVER['REQUEST_URI'] === '/login' || $_SERVER['REQUEST_URI'] === '/register') : ?>
<h1 class="logo text-center fluid-center"><a href="/">SCLZ</a></h1>
<?php else : ?>
<nav class="ui pointing purple menu">
    <a href="/" class="active item">
        <i class="home icon"></i> SCLZ
    </a>
    <div class="right menu">
        <?php if(isset($_SESSION['logged'])) : ?>
        <div class="item">
          <div class="ui icon input">
            <input type="text" placeholder="Search...">
            <i class="search link icon"></i>
          </div>
        </div>
        <a href="/settings" class="circular ui icon purple button">
            <i class="icon settings"></i>
        </a>
        <div class="item">
            <a href="/logout" class="ui red button logout-btn">Log-out</a>
        </div>
        <?php else : ?>
        <div class="item">
            <a href="/register" class="ui teal button register-btn">Register</a>
        </div>
        <div class="item">
            <a href="/login" class="ui button login-btn">Log-in</a>
        </div>
        <?php endif; ?>
    </div>
</nav>
<?php endif; ?>