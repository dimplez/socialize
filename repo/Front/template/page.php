<!DOCTYPE html>
<html class="<?php print $class; ?>">

<head>
    <title><?php $_($title); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php if(isset($meta) && is_array($meta)): ?>
    <?php foreach($meta as $name => $content): ?>
    <meta name="<?php print $name; ?>" content="<?php print $content; ?>" />
    <?php endforeach; ?>
    <?php endif; ?>

    <link rel="stylesheet" type="text/css" href="<?php $cdn(); ?>/styles/custom.css" />
    <!-- <link rel="stylesheet" type="text/css" href="<?php $cdn(); ?>/styles/bootstrap.css" /> -->
    <link rel="stylesheet" type="text/css" href="<?php $cdn(); ?>/styles/semantic.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php $cdn(); ?>/styles/awesome.css" />

    <!-- Google Fonts -->
    <link href='http://fonts.googleapis.com/css?family=Frijole' rel='stylesheet' type='text/css'>
    
    <script type="text/javascript" src="<?php $cdn(); ?>/scripts/jquery.js"></script>
    <!-- <script type="text/javascript" src="<?php $cdn(); ?>/scripts/bootstrap.js"></script> -->
    <script type="text/javascript" src="<?php $cdn(); ?>/scripts/semantic.min.js"></script>
    <script type="text/javascript" src="<?php $cdn(); ?>/scripts/custom.js"></script>
</head>

<body>
    <section class="page">
        <section class="head"><?php echo $head; ?></section>
        <section class="body"><?php echo $body; ?></section>
        <section class="foot"><?php echo $foot; ?></section>
    </section>
</body>
</html>