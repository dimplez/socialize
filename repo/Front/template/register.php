<div class="register small-center">
    <?php if(isset($error)) : ?>
    <div class="ui error message">
        <div class="header">Username is already taken</div>
        
        <p>Please use a different username.</p>
    </div>
    <?php endif; ?>
    <form role="form" method="POST" action="" class="ui form large segment fixed-center">
            <div class="field">
                <label for="full_name">Full Name</label>
                <div class="ui left labeled input">
                    <input id="full_name" type="text" placeholder="Full Name" name="full_name" 
                    <?php echo isset($error) ? 'value="'. $error['full_name'].'"' : NULL ; ?> required>
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>
                </div>
            </div>
            <div class="field <?php echo isset($error) ? 'error' : ''; ?>">
                <label for="username">Username</label>
                <div class="ui left labeled input">
                    <input id="username" type="text" placeholder="Username" name="username" 
                    <?php echo isset($error) ? 'value="'. $error['username'] .'"' : NULL ; ?> required>
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>
                    <?php if( isset($error) ) : ?>
                    <div class="ui red pointing above ui label">Username <?php echo $error['username']; ?> is already taken</div>
                    <?php endif; ?>
                </div>
            </div>
            <div class="field">
                <label for="email">Email</label>
                <div class="ui left labeled input">
                    <input id="email" type="email" placeholder="Email" name="email" 
                    <?php echo isset($error) ? 'value="'. $error['email'].'"' : NULL ; ?> required>
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>
                </div>
            </div>
            <div class="field">
                <label for="password">Password</label>
                <div class="ui left labeled input">
                    <input id="password" type="password" placeholder="Password" name="password" required>
                    <div class="ui corner label">
                        <i class="icon asterisk"></i>
                    </div>
                </div>
            </div>
        <div class="ui buttons">
            <a href="/login" class="ui button">Cancel</a>
            <div class="or"></div>
            <button type="submit" class="ui positive button">Register</button>
        </div>
    </form>
</div>