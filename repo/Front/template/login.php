<div class="login">
    <?php if(isset($login_error)) : ?>
    <div class="ui error message">
        <div class="header">Please re-enter your account</div>
        
        <p>The account you entered is incorrect. Please try again (make sure your caps lock is off).</p>

        <p>Forgot your password? <a href="#">Request a new one.</a></p>
    </div>
    <?php endif; ?>
    <div class="ui two column middle aligned relaxed grid basic segment">
        <div class="column">
            <form role="form" method="POST" action="" class="ui form large error segment">
                <div class="<?php echo isset($login_error) ? 'error' : NULL; ?> field">
                    <label for="username">Username</label>
                    <div class="ui left labeled icon input">
                        <input id="username" type="text" placeholder="Username" name="username" class="error" required>
                        <i class="user icon"></i>
                        <div class="ui corner label">
                            <i class="asterisk icon"></i>
                        </div>
                    </div>
                </div>
                <div class="<?php echo isset($login_error) ? 'error' : NULL; ?> field">
                    <label for="password">Password</label>
                    <div class="ui left labeled icon input">
                        <input id="password" type="password" name="password" required>
                        <i class="lock icon"></i>
                        <div class="ui corner label">
                            <i class="asterisk icon"></i>
                        </div>
                    </div>
                </div>
                <div class="ui buttons">
                    <a href="/forgot" class="ui button">Forgot?</a>
                    <div class="or"></div>
                    <button type="submit" class="ui positive button"><strong>Login</strong></button>
                </div>
            </form>
        </div>
        <div class="ui vertical divider">
            Or
        </div>
        <div class="column">
            <a href="/register">
                <div class="huge green ui labeled icon button">
                    <i class="signup icon"></i>
                    Register
                </div>
            </a>
        </div>
    </div>
</div>