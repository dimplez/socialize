<div class="settings small-center">
    <form class="ui fluid form segment" role="form" method="POST" action="" enctype="multipart/form-data">
        <div class="field">
            <label>Upload Image</label>
            <input type="file" name="profileimage">
            <img src="<?php echo isset($error) ? $error['profile'] : $settings['Profile']; ?>" class="profile">
        </div>

        <div class="field">
            <label for="full_name">Full Name</label>
            <input id="full_name" placeholder="Full Name" type="text" name="full_name" 
            <?php echo isset($error) ? 'value="'. $error['full_name'].'"' : 'value="'. $settings['Full Name'] .'"'; ?> required>
        </div>
        <div class="field <?php echo isset($error) ? 'error' : ''; ?>">
            <label for="username">Username</label>
            <input id="username" placeholder="Username" type="text" name="username"
            <?php echo isset($error) ? 'value="'. $error['username'] .'"' : 'value="'. $settings['Username'] .'"'; ?> required>
            <?php if( isset($error) ) : ?>
                <div class="ui red pointing above ui label">Username is already taken</div>
            <?php endif; ?>
        </div>

        <div class="field">
            <label for="email">Email</label>
            <input id="email" placeholder="Email" type="email" name="email"
            <?php echo isset($error) ? 'value="'. $error['email'] .'"' : 'value="'. $settings['Email'] .'"'; ?> required>
        </div>
        <div class="field">
            <label for="password">Password</label>
            <input id="password" placeholder="<?php echo isset($error) || isset($settings['Password']) ? '&bull;&bull;&bull;&bull;&bull;&bull;' : 'Password'; ?>" type="password" name="password">
        </div>

        <div class="ui horizontal icon divider">
            <i class="circular users icon"></i>
        </div>
        <hr>
        <button type="submit" class="ui positive submit button">Save</div>
    </form>
</div>